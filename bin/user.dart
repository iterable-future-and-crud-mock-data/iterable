bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool anyUserUnder13(Iterable<User> users) {
  return users.every((user) => user.age > 13);
}

Iterable<User> filterOutUnder21(Iterable<User> users) {
  return users.where((user) => user.age >= 21);
}

Iterable<User> findShortNamed(Iterable<User> users) {
  return users.where((user) => user.name.length <= 3);
}

Iterable<String> getNameAndAges(Iterable<User> users) {
  return users.map((user) => '${user.name} is ${user.age}');
}

void main() {
  var users = [
    User(name: 'ABCDE', age: 14),
    User(name: 'BCDEF', age: 18),
    User(name: 'CDE', age: 18),
    User(name: 'DEF', age: 21),
    User(name: 'E', age: 25)
  ];
  if (anyUserUnder18(users)) {
    print('Have any user under 18');
  }
  if (anyUserUnder13(users)) {
    print('Have users is over 13');
  }

  var AgeMoreThan21Users = filterOutUnder21(users);
  print('Age more than 21: $AgeMoreThan21Users');
  var shortNameUsers = findShortNamed(users);
  print('Shorted name users: $shortNameUsers');

  var nameAndages = getNameAndAges(users);
  print('Name And Age: $nameAndages');
  // for (var user in nameAndages){
  // print(users);
  // }
}

class User {
  String name;
  int age;
  User({
    required this.name,
    required this.age,
  });
  String toString() {
    return '$name, $age';
  }
}
